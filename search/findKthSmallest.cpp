#include <iostream>
#include <queue>

using namespace std;


int findKthSmallest(int arr[], int k, int SIZE){

    priority_queue<int> q;

    for(int i=0; i < k; ++i)
        q.push(arr[i]);

    for(int i=k; i < SIZE; ++i){
        int max = q.top();
        if( arr[i] < max)
            q.pop(), q.push(arr[i]);
    }
    return q.top();
}


int main(void){

    int arr[] = { 2, 2, 13, 4, 5};
    int k = 2;

    cout << "Kth smallest: " << findKthSmallest(arr, k, 5) << endl;

    return 0;
}
