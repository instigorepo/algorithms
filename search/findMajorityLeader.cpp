#include <stdio.h>
#include <stdlib.h>

// Finds the majority leader in the given list
//
int findMajorityLeader(int Arr[], int N){

    int n = 0;
    int cnt = 1;
    int i = 0;

    for(i = 1; i < N; ++i){
        if(Arr[i] == n)
            cnt++;
        else
            cnt--;

        if(cnt == 0) 
            n = Arr[i], cnt = 0;
    }
    return n;
}


int main(void){

    int Arr[] = {4,2,2,3,2,4,2,2,6,4};
    int N = sizeof(Arr)/sizeof(Arr[0]);
    printf("Majority Leader: %d\n", findMajorityLeader(Arr,N));

    return 0;
}
