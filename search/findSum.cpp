#include <iostream>

using namespace std;


// Name: bsearch
//
// Description: Performs a binary search in O(lg N)
// searching for a given value in the given array
//
bool bsearch(int *array, int value, int SIZE){

    int lo = 0, mid = 0, high = SIZE;
    
    while( lo <= high ){
        mid = ((unsigned int)lo + (unsigned int)high) >> 1;
     
        if( value < array[mid])
            high = mid -1;
        else if( value > array[mid])
            lo = mid + 1;
        else
            return true; 
    }
    return false;
}

// Name: findSum
//
// Description: Find the pair of values that sum to the given value S
//
void findSum(int *array, int S, int SIZE){

  
    for(int i = 0; i < SIZE; ++i){
        
        if( bsearch(array, S - array[i], SIZE))
            cout << "(" << S - array[i] << "," << array[i] << ")\n";
    }
}


int main(void){

    int arr[] = { 1, 3, 4, 13, 5, 6, 24};

    findSum(arr, 9, (sizeof(arr)/sizeof(arr[0]))-1);

    return 0;
}
