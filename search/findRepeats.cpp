#include <iostream>

using namespace std;


void findRepeats(int *list, int N){

    if( N <= 1) return;

    int prev = list[0];
   
    for(int i=1; i < N; ++i){

        if(prev == list[i])
            cout << prev << " ";
        
        prev = list[i];
    }
}

int main(void){

    int a[] = {1, 2, 3, 3, 4, 4, 5};
    int numItems = (size_t)(sizeof(a)/sizeof(a[0]));

    findRepeats(a, numItems);
    return 0;
}
