#include <stdio.h>
#include <stdlib.h>

typedef struct Node{
    struct Node *next;
    int data;
}Node;


Node *root = NULL;

Node* MK(int data){
   Node *newNode = (Node*)malloc(sizeof(Node));

   if(newNode){
       newNode->next = NULL;
       newNode->data = data;
   }
   return newNode;
}

Node * reverseEveryOther(Node *head){

    Node* curr = head;
    Node *next = NULL;
    Node *nextNext = NULL;

    if( head && head->next ) head = head->next;

    while(curr && curr->next){
        next = curr->next;
        nextNext = next->next;
        next->next = curr;
        curr->next = (!nextNext || !nextNext->next) ? nextNext : nextNext->next;
        curr = nextNext;
    }
    return head;
}

void printList(Node *head){
    
    Node *tmp = head;
    
    while(tmp){ printf("%d ", tmp->data); tmp = tmp->next; }
    printf("\n");

}


int main(void){

    Node *head = MK(1);
    head->next = MK(2);
    head->next->next = MK(3);
    head->next->next->next = MK(4);

    printList(head);
    head = reverseEveryOther(head); 

    while(head){
        printf("%d -> ", head->data);
        head = head->next;
    }
    printf("\n");

}
