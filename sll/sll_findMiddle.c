#include <stdio.h>
#include <stdlib.h>


// SLL Node
//
typedef struct Node{
    struct Node *next;
    int data;
}Node;

Node *root = NULL;

Node* Create(int item){
    Node *newNode = (Node*)malloc(sizeof(Node));

    if(newNode){
        newNode->next = NULL;
        newNode->data = item;
    }
    return newNode;
}

void insertItem(Node *head, int item){

    Node *newNode = Create(item);
    Node *tmp = head;
    Node *prev = NULL;

    // base case: no items
    //
    if(!tmp){
        root = newNode;
        return;
    }

    
    while(tmp){
        if( tmp->data > item ){
            break;
        }
        else{
            // increment prev looking for the correct position
            prev = tmp;
            tmp = tmp->next;
        }
    }
   
    if(prev){
        prev->next = newNode;
    }
    else{
        root = newNode;
    } 
    newNode->next = tmp;   
}

void printList(Node *head){

    Node *tmp = head;
    while(tmp ){
        printf("%d ", tmp->data);
        tmp = tmp->next;
    }
    printf("\n");
}

// Name: findMiddle
//
// Description: Find the middle of a given linked list
// We utilize two pointers to solve this in one pass (a slow pointer
// moving one node at a time and a fast pointer that moves 2 nodes at a time)
// Once the fast pointer reaches the end, we've found our middle item
//
Node *findMiddle(Node *head){

    Node *slow = head;
    Node *fast = head;

    while( (fast->next) != NULL && (fast->next->next) != NULL ){
        slow = slow->next;
        fast = fast->next->next;
    }
    return slow;
}

int main(void){

    insertItem(root,3);
    insertItem(root,2);
    insertItem(root,5);
    insertItem(root,1);
    insertItem(root,4);
    insertItem(root,6);
    printList(root);
    printf("middle item is: %d\n", ((Node*)findMiddle(root))->data);
    return 0;
}
