#include <stdio.h>
#include <stdlib.h>

typedef struct Node{
    struct Node *next;
    int data;
}Node;


Node *root = NULL;

Node* MK(int data){
   Node *newNode = (Node*)malloc(sizeof(Node));

   if(newNode){
       newNode->next = NULL;
       newNode->data = data;
   }
   return newNode;
}

Node * reverseListInChunks(Node *head, int sizeChunk){

    Node* currStart = head;
    Node* currEnd = NULL;
    Node *next = NULL;
    Node *prev = NULL;

    int i;
    while(currStart != NULL){
        for(i= 0, currEnd = currStart; i < (sizeChunk-1) && currEnd->next; ++i){
            currEnd = currEnd->next;
        }
        next = currEnd->next;
        currEnd->next = prev;
        prev = currStart, currStart = next;
    }
    return prev;
}

void printList(Node *head){
    
    Node *tmp = head;
    
    while(tmp){ printf("%d ", tmp->data); tmp = tmp->next; }
    printf("\n");

}


int main(void){

    Node *head = MK(1);
    head->next = MK(2);
    head->next->next = MK(3);
    head->next->next->next = MK(4);
    head->next->next->next->next = MK(5);
    head->next->next->next->next->next = MK(6);
    printList(head);
    head = reverseListInChunks(head, 3); 

    while(head){
        printf("%d ->", head->data);
        head = head->next;
    }
    printf("\n");

}
