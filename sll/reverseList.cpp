#include <stdio.h>
#include <stdlib.h>

typedef struct Node{
    struct Node *next;
    int data;
}Node;


Node *root = NULL;

Node* MK(int data){
   Node *newNode = (Node*)malloc(sizeof(Node));

   if(newNode){
       newNode->next = NULL;
       newNode->data = data;
   }
   return newNode;
}

Node * reverseList(Node *head){

    Node* curr = head;
    Node *next = NULL;
    Node *prev = NULL;

    while(curr != NULL){
        next = curr->next;
        curr->next = prev;
        prev = curr, curr = next;
    }
    return prev;
}

void printList(Node *head){
    
    Node *tmp = head;
    
    while(tmp){ printf("%d ", tmp->data); tmp = tmp->next; }
    printf("\n");

}


int main(void){

    Node *head = MK(1);
    head->next = MK(2);
    head->next->next = MK(3);
    head->next->next->next = MK(4);

    printList(head);
    head = reverseList(head); 

    while(head){
        printf("%d ->", head->data);
        head = head->next;
    }
    printf("\n");

}
