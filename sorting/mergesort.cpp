#include <cstdlib>
#include <iostream>

using namespace std;

void merge(int *array, int left, int right){

    int mid = ((unsigned int)left + (unsigned int)right) >> 1;
    int idx = 0;
    int i = left;
    int j = mid + 1;

    int *temp = new int[right-left+1];

    // sort the numbers in the left and right arrays
    while( i <= mid && j <= right){
        if(array[i] < array[j])
            temp[idx++] = array[i++];
        else
            temp[idx++] = array[j++];
    }

    // copy remaining items in left and right array
    while( i <= mid )  temp[idx++] = array[i++];
    while( j <= right) temp[idx++] = array[j++];

    // copy back the sorted items into the master array
    int iter;

    for(iter = 0; iter < idx; ++iter){
        array[iter + left] = temp[iter];
    }
    
    delete [] temp;

    return;
}



void merge_sort(int *input, int left, int right){

    if( left < right){
        int mid = left + (right-left) /2;
        merge_sort(input, left, mid);
        merge_sort(input, mid+1, right);
        merge(input, left, right);
    }
}



int main(void){


    int arr[10] = { 500,700, 800, 100, 300, 200, 900, 400, 1000, 600};
    int N = sizeof(arr)/sizeof(arr[0]);
    merge_sort(arr,0, N-1);
    
    for(int i=0; i < N-1; ++i){
        cout << arr[i] << " ";
    }
    cout << endl;
    return 0;
}
