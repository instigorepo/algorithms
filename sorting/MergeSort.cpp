#include <iostream>
#include <cstdlib>
#include <algorithm>

#define MERGESORT

void print(int *A, int N){

    for(int i = 0; i < N; ++i){
        std::cout << A[i] << " ";
    }
    std::cout << std::endl;
}


void merge(int *A, int p, int r){

    int mid = ((unsigned int)p + (unsigned int)r) >> 1;
    int idx = 0;
    int j = p;
    int k = mid +1;
   
    int *temp = new int[r-p+1];

    // create temp storage to store merged data
    while( j <= mid && k <= r){
        // compare the two halves of the array
        if( A[j] < A[k])
            temp[idx++] = A[j++];
        else
            temp[idx++] = A[k++];
        
    }

    // copy remaining items in left array
    while( j <= mid ){
         temp[idx++] = A[j++];
    }

    // copy remaining items in the right array
    while( k <= r ){
        temp[idx++] = A[k++];
    }

    // move items from temp array to master array 'A'
    for(int i=p; i <= r; ++i){
        A[i] = temp[i-p];
    }
    delete [] temp;
}


void merge_sort(int *A, int p, int r){
    
    if( p < r ){
        int mid = ((unsigned int)p + (unsigned int)r) >> 1;
        merge_sort(A,p, mid);
        merge_sort(A,mid+1,r);
        merge(A,p,r);
    }
}

bool validate_sort(int unsorted[], int sortedList[], int unsortedSize){

     bool isSorted = false;

     std::sort(unsorted, unsorted + unsortedSize);
    
     for(int i=0; i < unsortedSize; ++i){
         if(unsorted[i] == sortedList[i])
             isSorted = true;
     }
     return isSorted;
} 

int main(void){

    int listToSort[] = {23,21,3,1,9,14};
    int originalList[] = {23,21,3,1,9,14};
    int N = sizeof(listToSort)/sizeof(listToSort[0])-1;

    std::cout << "Unsorted List: ";
    print(originalList,N);
    std::cout << "Sorted List: ";
#ifdef MERGESORT
    merge_sort(listToSort, 0, N);
#endif
    print(listToSort,N);
    bool result = validate_sort(originalList, listToSort, N);
    if(result)
        std::cout << "Items are sorted" << std::endl;
    else
        std::cout << "Items are not sorted" << std::endl;

    return 0;
}
