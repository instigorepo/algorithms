#include <iostream>
#include <cstdlib>

using namespace std;

// Description: This snippet illustrates how to 
// reverse a singly linked list of values
//

typedef struct Node{
    int data;
    Node *next;
}Node;

// Name: MK
// 
// Description: Creates a new node
// 
// Returns: Ptr reference to new node, else NULL
//
Node *MK( int item ){
    Node *newNode = (Node*)malloc(sizeof(Node));
 
    if( NULL == newNode){
        perror("Insufficient memory to create node");
        exit(1);
    }
    newNode->next = NULL;
    newNode->data = item;
    
    return newNode;
}

Node* reverseSLL(Node *head){
    Node *curr = head;
    Node *prev = NULL;
    Node *next = NULL;

    // Initial Iteration setup
    //
    //   (NULL)  1 -> 2 -> 3 -> 4 -> 5 -> NULL 
    // prev--^
    // next--^
    // curr -----^
    // 
    while(curr){
        //
        // 1. Get the next node
        //
        //     (NULL) 1 -> 2 -> 3 -> 4 -> 5 -> NULL
        //  prev--^
        //  curr  ----^
        //  next  ---------^
        // 
        next = curr->next;

        // 2. Link current node's next to the previous node
        // 
        //     (NULL) <---- 1  2 -> 3 -> 4 -> 5 -> NULL
        // prev---^
        //        curr -----^
        //        next-----------^
        //  
        curr->next = prev;
 
        // 3. Update our pointers for the next iteration
        //        a. Set the previous ptr to our current item
        //        b. Set the current ptr to the next ptr
        //
        //    (NULL) <---- 1  2 ->  3 -> 4 -> 5 -> NULL
        //          prev---^
        //               curr---^ 
        //                     next---^    
        prev = curr, curr = next;

        // 4. Repeat Steps 1-3
    }
    return prev;
}

void printList(Node *head){

    Node *curr = head;

    while(curr){
        cout << curr->data << " ";
        curr = curr->next;
    }
    cout << endl;

}

int main(void){
    
    Node *reversedList = NULL;
    // create a quick list
    //
    Node *root = MK(1);
    root->next = MK(2);
    root->next->next = MK(3);
    root->next->next->next = MK(4);
    root->next->next->next->next = MK(5);

    cout << "List before: ";
    printList(root);

    reversedList = reverseSLL(root);
    cout << "List after:  ";
    printList(reversedList);

    return 0;
}

// Output: 
//     List before: 1 2 3 4 5 
//     List after:  5 4 3 2 1
