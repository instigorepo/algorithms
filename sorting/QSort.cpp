#include <iostream>
#include <algorithm>
using namespace std;


int partition(int list[], int L, int N)
{
    int swapIdx = L;
    int pos = L + N - 1;
    for (int i = L; i < L + N - 1; ++i) {
       if (list[i] < list[pos]) 
          std::swap(list[swapIdx++], list[i]);
    }

    std::swap(list[swapIdx], list[pos]);

    return swapIdx;
}

void qsort(int list[], int start, int end)
{
    if (start < end) {
        // Use end as the pivot:
        int idx = partition(list, start, end - start + 1);

        qsort(list, start, idx - 1);
        qsort(list, idx + 1, end);
    }
}

int main()
{
    int list[] = { 3, 1, 7, 2, 8, 6, 4, 9, 5, 0 };
    qsort(list, 0, 9);
    cout << "Printing..." << endl;

    for (unsigned int i = 0; i < 10; ++i) {
        cout << list[i] << " ";
    }
    cout << endl;

    return 0;
}

