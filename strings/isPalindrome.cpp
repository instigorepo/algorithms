#include <iostream>

using namespace std;

// 
// Name: invert
// 
// Description: Invert the given number
//
int invert(int x){

    int y = 0;
 
    while(x){
        y = y * 10 + x % 10;
        x /= 10;
    }
    return y;
}

//
// Name: isPalindrome
//
// Description: Determine if the given number is a Palindrome
//
bool isPalindrome(int x){

    int res = x - invert(x);
    return res == 0;
}

int main(void){

    int x = 999, y = 411;

    if(isPalindrome(x))
        cout << "Given number '" << x << "' is a Palindrome\n";
    else
        cout << "Given number '" << x << "' is not a Palindrome\n";

    if(isPalindrome(y))
        cout << "Given number '" << y << "' is a Palindrome\n";
    else
        cout << "Given number '" << y << "' is not a Palindrome\n";

    return 0;
}

