#include <iostream>

using namespace std;


bool isAnagram(const char strA[], const char strB[]){

    bool A[256] = {0};
    bool B[256] = {0};

    int sizeA = strlen(strA);
    int sizeB = strlen(strB);

    for(int i=0; i < sizeA; ++i)
        A[strA[i] -'a']++;

    for(int i=0; i < sizeB; ++i)
        B[strB[i] -'a']++;

    for(int i = 0; i < 256; ++i){
        if( A[i] != B[i]){
            return false;
        }
    }
    return true;   
}



int main(void){

    char B[] = "ab";
    char A[] = "ba";

    if(isAnagram(A,B)){
        cout << "The two arrays are anagrams\n";
    }
    else{
        cout << "The two arrays are not anagrams\n";
    }

    return 0;
}
