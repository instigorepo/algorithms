#include <iostream>
#include <stack>
#include <string>

using namespace std;


bool checkBrackets(string str){

    stack<char> s;
    char inverse[3] = {
        ['{'] = '}',
        ['('] = ')',
        ['['] = ']'
    };

    for(int i=0; i < str.size(); ++i){
        
        if(str[i] == '(' || str[i] == '{' || str[i] == '['){
            s.push_back(str[i]);
        }
        else if( str[i] == ')' || str[i] == '}' || str[i] == ']'){
            if( s.empty() || s.top() != inverse[str[i]])
                return false;
            s.pop();
        }
    }
    return s.empty();
}


int main(void){

    string brackets("{()[]}");

    if(checkBrackets(brackets)){
        cout << "Brackets are in the correct format" << endl;
    }
    return 0;
}
