#include <iostream>
#include <cstring>


inline void swap(char *a, char *b){
    char tmp = *a;
    *a = *b;
    *b = tmp;
}

void perm(char arr[], int N, int K){

    if( N == 1 || N == K){
        std::cout << arr << std::endl;
        return;
    }

    for(int i = K; i < N; ++i){
        swap(&arr[i], &arr[K]);
        perm(arr, N, K +1);
        swap(&arr[i], &arr[K]); // backtrack
    }
}

int main(void){

    char arr[] = "Hustler";

    perm(arr, strlen(arr),0);
    
    return 0;
}
