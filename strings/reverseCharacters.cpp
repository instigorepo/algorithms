#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* Write a method that takes an array of characters and returns an array of the same characters in reverse order
 * with every consonant between 'a' and 'z' in lowercase and every vowel between 'a' and 'z' in uppercase. */


inline void swap(char *a, char *b){
    
    char tmp = *a;
    *a = *b;
    *b = tmp;
}


inline char* convertChar(char *c ){

    if( *c == 'a' || *c == 'e' || *c == 'i' || *c == 'o' || *c == 'u'){
        // handle vowels
        if( *c >= 97 && *c <= 122)
            *c = *c  - 32;
    }
    else{
        // handle consonants
        if( *c >= 65 && *c <= 90 ) 
            *c = *c + 32;
    }
    return c;
}

void foo(char *str){

    int i = 0, j = strlen(str)-1;

    while( i < j ){
        swap(convertChar(&str[i++]), convertChar(&str[j--]));
    }
    
    if( i == j ){
        convertChar(&str[i]);
        return;
    }
}

int main(void){

    char tmp[20] = "bac";
    foo(tmp);
    printf("%s\n", tmp);

    return 0;
}
