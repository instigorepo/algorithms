#include <iostream>
#include <cstdlib>
#include <cstdio>

using namespace std;


typedef struct Node{
    struct Node *left, *right;
    int data;
}Node;

Node *root = NULL;

// Name: InsertNode
//
void InsertNode(Node **tree, int item){

    Node *newNode = (Node*)malloc(sizeof(Node));
    Node **tmp = tree;

    if(newNode){
        newNode->left = NULL, newNode->right = NULL;
        newNode->data = item;
    }

    while(*tmp){
        if((*tmp)->data > item){
            tmp = (&(*tmp)->left);
        }
        else if((*tmp)->data < item){
            tmp = (&(*tmp)->right);
        }
        else{
            return;  // same item
        }
    }
    *tmp = newNode;
}


// Name: findKthLargest
//
void findKthLargest(Node *tree, int kthLargest, int *result){

    static int count = 0;

    if(tree){
        // perform post order
        findKthLargest(tree->right, kthLargest, result);
        count++;
        if(count == kthLargest)
            *result = tree->data;

        findKthLargest(tree->left, kthLargest, result);
    }
}

void InOrderPrint(Node *tree){

    if(tree){
        if(tree->left) InOrderPrint(tree->left);
        cout << tree->data << " ";
        if(tree->right) InOrderPrint(tree->right);

    }
}

int main(void){

    int k = 0;
    InsertNode(&root,1);
    InsertNode(&root,5);
    InsertNode(&root,4);
    InsertNode(&root,9);
    InsertNode(&root,6);
    InOrderPrint(root);
    findKthLargest(root, 2, &k);
    printf("\nKth largest: %d\n", k);
    return 0;
}
