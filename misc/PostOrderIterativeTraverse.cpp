#include <iostream>
#include <stack>
#include <cstdlib>

using namespace std;

template <class T> struct Node{
    Node<T>*left,*right;
    T data;

};

template <class T>
Node<T>* createNode(T data){
    Node<T>* tmp = NULL;

    tmp = (Node<T>*)malloc(sizeof(Node<T>));
    if(tmp){
        tmp->data = data;
        tmp->left = NULL, tmp->right = NULL;
    }
    return tmp;
}


template <class T>
void PostOrderIterativeTraverse(Node<T> *head){

    stack<Node<T> *> s;
    Node<T> *prev = NULL;
    
    if(!head) return;

    s.push(head);
    while(!s.empty()){
        Node<T> *n = (Node<T>*)s.top();
        if( !prev || prev->left == n || prev->right == n){
            if(n->left) s.push(n->left);
            else if(n->right) s.push(n->right);
        }
        else if( n->left == prev){
            if(n->right) s.push(n->right);
        }
        else{
            // pop item and print
            cout << n->data << " ";
            s.pop();
        }
        prev = n;
    } 
    cout << endl;   
}

int main(void){

    Node<int>* n1 = createNode(6), *n2 = createNode(7)
        , *n3 = createNode(3), *n4 = createNode(2), *n5 = createNode(4);
    n1->left = n3, n1->right = n2;
    n3->left = n4, n3->right = n5;
    PostOrderIterativeTraverse(n1);
    return 0;
}
