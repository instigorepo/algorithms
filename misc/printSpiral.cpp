#include <iostream>

using namespace std;

#define R 3
#define C 6

void spiralPrint(int a[R][C], int m, int n){
    int i, k= 0, l = 0;

    while(k < m && l < n ){
        
        // 1. print first row
        for(i = l; i < n; ++i)
            cout << a[k][i] << " "; 

        k++;  // move to the next row

        // 2. print the last column
        for(i=k; i < m; ++i)
           cout << a[i][n-1] << " "; 

        // go to the left
        n--;

        // 3. print the last row
       if( k < m ){
           for(i = n-1; i >= l; i--)
               cout << a[m-1][i] << " ";
           
           // go up a row
           m--; 
       }
       // 4. print the first column
       if( l < n ){
           for(i = m-1; i >= k; i--)
               cout << a[i][l] << " ";
           l++;
       }
    }
    cout << endl;
}


int main(void){

    int a[R][C] = {{1, 2, 3, 4, 5, 6 },
                  {7, 8, 9, 10, 11, 12},
                  {13, 14, 15, 16, 17, 18}};

    spiralPrint(a,R,C); 


    return 0;
}
// output: 1 2 3 4 5 6 12 18 17 16 15 14 13 7 8 9 10 11 

